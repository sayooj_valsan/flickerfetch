package com.fortytwo.adapter;

import com.android.volley.toolbox.NetworkImageView;
import com.fortytwo.flicker.Constants;
import com.fortytwo.flicker.FlickerApplication;
import com.fortytwo.flicker.R;
import com.fortytwo.model.Photo;
import com.fortytwo.network.DataHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class PhotoAdapter extends BaseAdapter {

	Context context;

	public PhotoAdapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		return DataHelper.getIntance().getAllPhotos().size();
	}

	@Override
	public Photo getItem(int position) {
		return DataHelper.getIntance().getAllPhotos().get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Using ViewHolder Pattern
		ViewHolder viewHolder;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.rowlayout, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.mPhoto = (NetworkImageView) convertView.findViewById(R.id.image);
			//Set placeholder image
			viewHolder.mPhoto.setDefaultImageResId(R.drawable.placeholderimage);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();

		}
		
		String server = getItem(position).getServer().trim();
		String id = getItem(position).getId().trim();
		String secret = getItem(position).getSecret().trim();
		Integer farm = getItem(position).getFarm();
		StringBuffer url = new StringBuffer().append(Constants.HTTP_FARM).append(farm.intValue()).append(Constants.STATICFLICKR_COM).append(server)
				.append(Constants.STRING_SLASH).append(id).append(Constants._).append(secret).append(Constants._C_JPG);
		viewHolder.mPhoto.setImageUrl(url.toString(), ((FlickerApplication)context).getImageLoader());
		return convertView;
	}

	static class ViewHolder {
		NetworkImageView mPhoto;
	}

}
