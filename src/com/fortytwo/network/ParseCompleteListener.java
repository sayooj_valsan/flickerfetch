package com.fortytwo.network;

import java.util.ArrayList;

import com.fortytwo.model.Photo;

public interface ParseCompleteListener {
	
	/**
	 * onParseCompleteListener lets us know if parsing was success or not
	 * @param success if we successully parsed the data.
	 */
	public void onParseCompleteListener(ArrayList<Photo> photos);

}
