package com.fortytwo.network;

import java.util.ArrayList;

import com.fortytwo.model.Photo;



/**
 * Singleton DataHelper. Takes care of storing data
 * @author Sayooj Valsan
 *
 */
public class DataHelper {


	private static  DataHelper INSTANCE = new DataHelper();
	private static ArrayList<Photo> mPhotos = new ArrayList<Photo>();

	public static DataHelper getIntance(){
		return INSTANCE;
	}

	//Enforce Singleton
	private DataHelper(){

	}

	public void addPhoto(Photo Photo){
		mPhotos.add(Photo);
	}


	public void addAllPhotos(ArrayList<Photo> Photo){
		//Clear old values
		mPhotos.clear();
		mPhotos.addAll(Photo);
	}

	
	public void clearAll(){
		mPhotos.clear();
	}

	public ArrayList<Photo> getAllPhotos(){
		return mPhotos;
	}

}
