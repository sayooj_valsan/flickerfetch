package com.fortytwo.network;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fortytwo.model.Photo;

/**
 * VolleyHelper takes care of network request.
 * 
 * @author Sayooj Valsan
 * 
 */
public class FlickerParser {
	private static final String TITLE = "title";

	private static final String SECRET = "secret";

	private static final String SERVER = "server";

	private static final String OWNER = "owner";

	private static final String ISFAMILY = "isfamily";

	private static final String FARM = "farm";

	private static final String ID = "id";

	private static final String PHOTO = "photo";

	private static final String PHOTOS = "photos";

	private String TAG = FlickerParser.class.getSimpleName();

	RequestQueue queue;
	ParseCompleteListener listener;
	String url;
	ArrayList<Photo> mList = null;

	public FlickerParser(final String url, RequestQueue queue) {

		this.url = url;
		this.queue = queue;
	}

	public void execute(final ParseCompleteListener listener) {
		this.listener = listener;
		// Create Volley Network Request
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(
				Request.Method.GET, url, null, new Listener<JSONObject>() {

					@Override
					public void onResponse(final JSONObject response) {
						// Start a new Thread to take care of parsing. Since our
						// data is huge it makes sense to do this in a
						// background thread
						Thread d = new Thread(new Runnable() {

							@Override
							public void run() {
								// Parse the data
								try {
									JSONObject photos = response
											.getJSONObject(PHOTOS);

									JSONArray photosArray = photos
											.getJSONArray(PHOTO);

									int len = photosArray.length();
									 mList = new ArrayList<Photo>(
											len);

									for (int i = 0; i < len; i++) {
										JSONObject jsonObject = photosArray
												.getJSONObject(i);

										Photo photo = new Photo();

										photo.setId(jsonObject.getString(ID));

										photo.setFarm(jsonObject.getInt(FARM));
										photo.setIsfamily(jsonObject
												.getInt(ISFAMILY));
										photo.setOwner(jsonObject
												.getString(OWNER));
										photo.setServer(jsonObject
												.getString(SERVER));
										photo.setTitle(jsonObject
												.getString(TITLE));
										photo.setSecret(jsonObject
												.getString(SECRET));
										mList.add(photo);

									}
								//	DataHelper.getIntance().addAllPhotos(mList);

								} catch (JSONException e) {
									Log.e(TAG, e.getMessage());
									FlickerParser.this.listener
									.onParseCompleteListener(null);
									return;
								}

								if (FlickerParser.this.listener != null) {
									FlickerParser.this.listener
											.onParseCompleteListener(mList);
								}
							}
						});
						d.start();
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(TAG, error.getMessage());
						if (FlickerParser.this.listener != null) {
							FlickerParser.this.listener
									.onParseCompleteListener(null);
						}
					}
				});

		queue.add(jsObjRequest);
	}

	public void removeListener() {
		this.listener = null;
	}
}
