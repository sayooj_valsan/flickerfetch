package com.fortytwo.flicker;

import android.app.Application;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.android.volley.toolbox.Volley;

/**
 * Our Main Application
 * @author Sayooj Valsan
 *
 */
public class FlickerApplication extends Application {

	RequestQueue queue;
	private ImageLoader mImageLoader;
	@Override
	public void onCreate() {
		
		super.onCreate();
	}
	
	/**
	 * Get Volley's Request Queue
	 * @return RequestQueue
	 */
	public RequestQueue getVolleyRequestQueue()
	{
		if(queue == null)
		{
			 queue = Volley.newRequestQueue(this);
			 
		}
		return queue;
	}
	
	/**
	 * Get Volley's Image Loader
	 * @return
	 */
	public ImageLoader getImageLoader()
	{
		if(mImageLoader == null)
		{
			 mImageLoader = new ImageLoader(getVolleyRequestQueue(), new ImageCache() {
				 private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(14 * 1024 * 1024);
				@Override
				public void putBitmap(String url, Bitmap bitmap) {
					mCache.put(url, bitmap);					
				}
				
				@Override
				public Bitmap getBitmap(String url) {
					return mCache.get(url);
					
				}
			});
		}
		return mImageLoader;

	}
}
