package com.fortytwo.flicker;

public class Constants {
	public static final String _C_JPG = "_c.jpg";
	public static final String _ = "_";
	public static final String STRING_SLASH = "/";
	public static final String STATICFLICKR_COM = ".staticflickr.com/";
	public static final String HTTP_FARM = "http://farm";

	public static final String URL = "http://api.flickr.com/services/rest/?method=flickr.photos.getRecent&per_page=100&nojsoncallback=1&format=json&tags=&api_key=4ef2fe2affcdd6e13218f5ddd0e2500d";
}
