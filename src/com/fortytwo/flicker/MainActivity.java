package com.fortytwo.flicker;

import java.util.ArrayList;

import com.fortytwo.adapter.PhotoAdapter;
import com.fortytwo.model.Photo;
import com.fortytwo.network.DataHelper;
import com.fortytwo.network.ParseCompleteListener;
import com.fortytwo.network.FlickerParser;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.ListView;

public class MainActivity extends Activity implements ParseCompleteListener {

	private PhotoAdapter adapter;
	private ProgressDialog barProgressDialog;
	private AlertDialog noInternetDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final ListView listview = (ListView) findViewById(R.id.listview);
		adapter = new PhotoAdapter(getApplicationContext());
		listview.setAdapter(adapter);
		// If we dont have any data, trigger data fetch
		if (DataHelper.getIntance().getAllPhotos().isEmpty()) {
			fetchData();
		}
	}

	private void fetchData() {
		// Check for internet connection
		if (!Utils.isNetworkAvailable(getApplicationContext())) {
			showNoInternetDialog();
			return;
		}
		showProgress();
		FlickerParser helper = new FlickerParser(Constants.URL,
				((FlickerApplication) getApplication()).getVolleyRequestQueue());
		helper.execute(this);
	}

	/**
	 * Show progress dialog
	 */
	private void showProgress() {

		if (barProgressDialog != null)
			barProgressDialog.dismiss();
		barProgressDialog = new ProgressDialog(MainActivity.this);
		barProgressDialog.setTitle(R.string.downloading);
		barProgressDialog.setMessage(getString(R.string.downloading_progress));
		barProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		barProgressDialog.show();

	}

	@Override
	public void onDetachedFromWindow() {
		if (barProgressDialog != null && barProgressDialog.isShowing()) {
			barProgressDialog.dismiss();
		}
		super.onDetachedFromWindow();
	}

	@Override
	public void onParseCompleteListener(final ArrayList<Photo> photos) {
		
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// if progress bar is showing dismiss
				if (barProgressDialog != null)
					barProgressDialog.dismiss();
				if(photos == null)
					return;
				DataHelper.getIntance().addAllPhotos(photos);
				// Notify our adapter
				adapter.notifyDataSetChanged();

			}
		});

	}

	/**
	 * Show Alert dialog showing no internet connection messages
	 */
	private void showNoInternetDialog() {

		if(noInternetDialog != null)
			noInternetDialog.dismiss();
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle(R.string.no_internet_head);
		alertDialogBuilder.setMessage(R.string.no_internet_desc);
		// null should be your on click listener
		alertDialogBuilder.setPositiveButton(R.string.yes,
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (Utils.isNetworkAvailable(getApplicationContext())) {
							fetchData();
						} else {
							showNoInternetDialog();
						}

					}
				});
		alertDialogBuilder.setNegativeButton(R.string.cancel,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						finish();
					}
				});
		noInternetDialog = alertDialogBuilder.create();
		noInternetDialog.show();

	}

}
